/*
Bài 1
*/
document.getElementById("tinhtienthue").onclick = tinhthue;

function tinhthue() {
  var name = document.getElementById("name").value,
    tong = document.getElementById("thunhap").value - 4e6 - 16e5 * document.getElementById("nguoiphuthuoc").value,
    ketqua = 0;


  if (tong > 0 && tong <= 6e7) {
    ketqua = .05 * tong
  }
  else if (tong > 6e7 && tong <= 12e7) {
    ketqua = .1 * tong
  }
  else if (tong > 12e7 && tong <= 21e7) {
    ketqua = .15 * tong
  }
  else if (tong > 21e7 && tong <= 384e6) {
    ketqua = .2 * tong
  }
  else if (tong > 384e6 && tong <= 624e6) {
    ketqua = .25 * tong
  }
  else if (tong > 624e6 && tong <= 96e7) {
    ketqua = .3 * tong
  }
  else if (tong > 96e7) {
    ketqua = .35 * tong
  }
  else {
    alert("Số tiền thu nhập không hợp lệ")
  }
  ketqua = new Intl.NumberFormat("vn-VN").format(ketqua)
  document.getElementById("txttinhtienthue").innerHTML = "Họ tên: " + name + "; Tiền thuế thu nhập cá nhân: " + ketqua + " VND"
}

/*
Bài 2
*/

document.getElementById("tinhtiencap").onclick = tinhtiencap;

function disableInput() {
  var chonkhachhang = documnet.getElementById("chonkhachhang").value;
  document.getElementById("soketnoi").style.display = "congty" == chonkhachhang ? "block" : "none"
}

function tinhtong(chonkhachhang, makhachhang, sokenhcaocap, soketnoi, tiencap, l) {
  var tong = chonkhachhang + makhachhang + sokenhcaocap * soketnoi;
  return tiencap > 10 && (tong += (tiencap - 10) * l), tong


}

function tinhtiencap() {
  var chonkhachhang = document.getElementById("chonkhachhang").value,
    makhachhang = document.getElementById("makhachhang").value,
    sokenhcaocap = document.getElementById("sokenh").value,
    soketnoi = document.getElementById("soketnoi").value,
    tiencap = 0;

  if ("congty" == chonkhachhang) {
    tiencap = tingtong(15, 75, 50, sokenhcaocap, soketnoi, 5)
  }
  else if ("user" == chonkhachhang) {
    tiencap = tinhtong(4.5, 20.5, 7.5, sokenhcaocap, 0, 0)
  }
  else {
    alert("Hãy chọn loại khách hàng")
  }

  document.getElementById("txttinhtiencap").innerHTML = "Mã khách hàng: " + makhachhang + " Tiền cáp: " + new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD"
  }).format(tiencap)

}



